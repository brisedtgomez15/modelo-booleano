package Modelo;

import Entidad.TrabajoDeImpresion;
import java.util.PriorityQueue;
import java.util.Queue;

public class SistemaImpresion {

    Queue<TrabajoDeImpresion> trabajo = new PriorityQueue<TrabajoDeImpresion>();

    public void addTrabajo(int hoja, long time) {

        this.trabajo.add(new TrabajoDeImpresion(hoja,time));

    }

    public String getCola() {
        String cq = "";
        while (!this.trabajo.isEmpty()) {
            TrabajoDeImpresion temp=this.trabajo.poll();
            cq += "\n -Cantidad de hojas: "+temp.getcHoja()+" Tiempo de creación: "+temp.getTime()+".";
            
        }
        return cq;
    }
}
