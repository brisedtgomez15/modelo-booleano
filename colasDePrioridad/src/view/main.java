/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import Modelo.SistemaImpresion;
import java.util.Scanner;

public class main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Cantidad de trabajos: ");
        int a = sc.nextInt();
        SistemaImpresion sistema = new SistemaImpresion();

        while (a > 0) {
            System.out.println("Cantidad de hojas: ");

            int d = sc.nextInt();
            System.out.println("Tiempo de creación: ");
            long e = sc.nextLong();
            sistema.addTrabajo(d, e);
            
            a--;
            
        }
        System.out.println("Cola de prioridad: " + sistema.getCola());
    }
}
