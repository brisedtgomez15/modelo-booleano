package modelobooleano;

import java.util.*;

public class ModeloBooleano {

    public static String booleano(Stack<Integer> a, Stack<Integer> b) {
        if (a.size() > b.size()) {
            return "no esta contenido";
        }
        while (!a.isEmpty()) {
            int c = a.pop();
            if (b.size() > 0) {
                if (b.pop() != c) {
                    a.add(c);
                }
            }else {
               return "no esta contenido";
            }
        }
        return "está contenido";
    }

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        Stack<Integer> pPeq = new Stack();
        Stack<Integer> pGra = new Stack();
        System.out.println("tamaño pila pequeña: ");
        int a = leer.nextInt();
        for (int i = 0; i < a; i++) {
            pPeq.add(leer.nextInt());
        }
        System.out.println("tamaño pila grande: ");
        int b = leer.nextInt();
        for (int i = 0; i < b; i++) {
            pGra.add(leer.nextInt());
        }
        System.out.println(booleano(pPeq, pGra));
    }
}
